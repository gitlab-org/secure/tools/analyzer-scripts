#!/bin/sh

set -eu

if head -n 1 /etc/os-release | grep -q "Alpine Linux"; then
    apk --no-cache add g++ go
    apk add --update --no-cache --repository "https://dl-cdn.alpinelinux.org/alpine/v3.17/community" "delve"

    # Alpine ships with muslc which needs to be symlinked to glibc for Go and
    # Delve to work correctly. The filename differs depending on the CPU
    # architecture. See https://stackoverflow.com/a/35613430.
    if uname -a | grep -q "x86_64"; then
        mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
    else
        ln -s /lib/libc.musl-aarch64.so.1 /lib/ld-linux-aarch64.so.1
    fi
elif head -n 1 /etc/os-release | grep -q "Red Hat"; then
    yum install -y wget gcc
else
    # Assume Debian as the fallback.
    apt update && apt install -y wget gcc golang delve
fi
