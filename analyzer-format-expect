#! /usr/bin/env bash

set -eo pipefail

if [[ -z $1 || -z $2 ]]; then
	echo "Usage: analyzer-format-expect <actual_report_path> <expected_report_path>"
	exit 1
fi

JQ_FILTER="del(.version) |
           del(.scan.analyzer.version) |
           del(.scan.scanner.version) |
           del(.scan.start_time) |
           del(.scan.end_time) |
           del(.vulnerabilities[].id) |
           del(.vulnerabilities[].location.dependency.iid) |
           del(.dependency_files) |
           del(.remediations[]?.fixes[].id) |
           .vulnerabilities |= map_values(.links |= (. // [])) |
           .vulnerabilities |= map_values(.identifiers |= (. // [])) |
           .vulnerabilities |= map_values(.tracking |= (. // {})) |
           .vulnerabilities[]?.tracking.items |= [] |
           .vulnerabilities[]?.flags |= [] |
           del(.dependency_files[]?.dependencies[].dependency_path[]?.iid) |
           .vulnerabilities |= sort"

ACTUAL_REPORT_PATH=$1
EXPECTED_REPORT_PATH=$2

echo "Replacing '$EXPECTED_REPORT_PATH' with formatted version of '$ACTUAL_REPORT_PATH'"

jq -e "$JQ_FILTER" "$ACTUAL_REPORT_PATH" > "$EXPECTED_REPORT_PATH"
