#! /usr/bin/env bash

TEST_APP=$1
shift
IMAGE=$1
shift

if [[ -z "$TEST_APP" ]]; then
  echo "Usage: $0 TEST_APP_PATH [DOCKER_IMAGE]"
  exit 1
fi
  
if [[ ! -d "$TEST_APP" ]]; then
  echo "$TEST_APP does not exist"
  exit 1
fi

TEST_APP=$(cd "$TEST_APP" || exit 1; pwd)

if [[ -z "$IMAGE" ]]; then
  IMAGE=$(basename "$(pwd)"):$(git rev-parse --abbrev-ref HEAD)
  IMAGE="$(echo "$IMAGE" | sed 's/\//_/g')"
  echo image: "$IMAGE"
fi


# Add platform tag to build
PLATFORM=""
current_dir=$(basename "$PWD")
if [ "$current_dir" == "pmd-apex" ]; then
  PLATFORM="--platform=linux/amd64"
fi

docker run \
  --rm \
  -it \
  $PLATFORM \
  -e SAST_EXCLUDED_PATHS \
  -e GITLAB_FEATURES \
  -e SECURE_LOG_LEVEL \
  -e CI_PROJECT_DIR=/tmp/app \
  -v "$TEST_APP":/tmp/app \
  -v "$(pwd)":/tmp/current_dir \
  -w /tmp/app \
  "$IMAGE" "$@"
