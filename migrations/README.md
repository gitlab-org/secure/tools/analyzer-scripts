
# Migrations

Directory of one-off migration tasks. These may be useful in the future but were
designed for specific usecases such as data migrations

## `bulk_modify_analyzers.rb`

This script performs a bulk modification against GitLab repositories of your choosing.
You specify a list of repositories and branches to check out, implement a
callback function to modify each local checkout (for instance, manipulating files
with `sed`), and provide values to populate the commit message and MR. The
script runs through all repositories specified and makes the same change to each
project.

To use this script:

1. Generate a GitLab Personal Access Token and add it to a file named
   `.gitlab-access-token` in this directory.
2. Edit `update_analyzers.rb`. Look at the `@config` hash and modify its values
   accordingly. The `modify_fn` property is the callback function that will be
   executed for each repository being updated.
3. Run the script:
   ```shell
   ./bulk_modify_analyzers.rb
   ```

NB: it's useful to test your `modify_fn` implementation against a fork to reduce
the risk of polluting the MR history.
