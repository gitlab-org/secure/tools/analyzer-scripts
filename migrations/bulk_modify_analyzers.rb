#! /usr/bin/env ruby

# frozen_string_literal: true

require 'logger'
require 'rubygems'
require 'open-uri'
require 'gitlab'
require 'base64'
require 'net/http'
require 'uri'

@logger = Logger.new($stdout)

# This is a sample implementation of @config[:modify_fn].
# Modify anything you wish within `root_dir` and return an array of modified files
# relative to `root_dir`.
def demo(project_name:, branch:, mr_number:, root_dir:)
  @logger.debug("Modifying branch '#{branch}' of '#{project_name}' checked out at '#{root_dir}'...")

  shell(cmd: 'touch FOO', workdir: root_dir)

  ['FOO']
end

# Bumps the common package from v2 to v3.
def bump_common(project_name:, branch:, mr_number:, root_dir:)
  old_pkg = 'gitlab.com\/gitlab-org\/security-products\/analyzers\/common\/v2'
  new_pkg = 'gitlab.com\/gitlab-org\/security-products\/analyzers\/common\/v3'

  # Delete the package from go.mod
  # Why -i.bak? See https://stackoverflow.com/questions/5171901/find-and-replace-in-file-and-overwrite-file-doesnt-work-it-empties-the-file
  shell(cmd: "gsed -i.bak 's/.*#{old_pkg}.*//' go.mod",
        workdir: root_dir)

  # Install the updated package
  shell(cmd: "go get -u #{new_pkg}",
        workdir: root_dir)

  # Replace import paths
  # https://stackoverflow.com/questions/6758963/find-and-replace-with-sed-in-directory-and-sub-directories
  shell(cmd: "find . -name \"*.go\" -exec gsed -i.bak 's/#{old_pkg}/#{new_pkg}/g' {} \\;",
        workdir: root_dir)

  # Clean up go.mod
  shell(cmd: 'go mod tidy',
        workdir: root_dir)

  # Validate
  shell(cmd: 'go test ./...',
        workdir: root_dir)

  # Update changelog
  semver_str = nil
  File.readlines("#{root_dir}/CHANGELOG.md").each { |line|
    if line.start_with?('## ') && semver_str.nil?
      semver_str = line.split(' ')[1]
    end
  }

  # Minor bump
  semver = semver_str.split('.')
  semver[1] = semver[1].to_i + 1

  changelog_entry = "## #{semver.join('.')}\\n- Upgrade the `common` package to `v3` to support globstar patterns (!#{mr_number})\\n"

  shell(cmd: "gsed -i.bak '0,/^##.*/s/^##.*/#{changelog_entry}\\n&/' CHANGELOG.md",
        workdir: root_dir)

  ['go.mod', 'go.sum', './\*.go', 'CHANGELOG.md']
end

# Configuration options for the bulk update script. These values will be
# applied for each project being updated.
@config = {
  projects: [
    { path: 'jamesliu-gitlab/semgrep', branch: 'main' }
  ],
  commit_message: 'Bump common to v3',
  commit_branch: 'jliu/bulk-bump-common-v3',
  mr: {
    title: 'Draft: [Bulk Update] Bump common from v2 to v3',
    author: 'jamesliu-gitlab',
    description: %(
## What does this MR do?

Bumps the `common` library from v2 to v3.

## What are the relevant issue numbers?

- https://gitlab.com/gitlab-org/gitlab/-/issues/224440

/label ~"section::sec" ~"group::static analysis" ~"devops::secure" ~"Category:SAST" ~"type::maintenance"
/assign @jamesliu-gitlab
/assign_reviewer @jamesliu-gitlab
)
  },
  # Callback that's executed for each entry in @config[:projects]. Implement your own
  # method and reference it here. Check the `demo` method for the signature.
  modify_fn: method(:bump_common)
}

# fetch project id using the API. Could also just scrape the page, but that won't allow us
# to fetch confidential issues
def fetch_project_id(project_name)
  project_path = CGI.escape(project_name)
  url = "https://gitlab.com/api/v4/projects/#{project_path}"

  project_details = JSON.parse(URI.open(url).read)
  project_details['id']
end

def checkout_path(project_name:)
  Pathname.new('.checkouts').join(project_name)
end

def shell(cmd:, workdir: nil)
  workdir && @logger.debug("Using '#{workdir}' as the working directory.")
  @logger.debug("Executing shell command '#{cmd}'...")

  if workdir
    system("cd #{workdir} && #{cmd}", exception: true)
  else
    system(cmd, exception: true)
  end
end

def mr_exists(token:, project_id:, mr_author:, mr_title:, branch_name:)
  uri = URI.parse("https://gitlab.com/api/v4/projects/#{project_id}/merge_requests?state=opened&author_username=#{mr_author}&search=#{mr_title}&source_branch=#{branch_name}")
  request = Net::HTTP::Get.new(uri)
  request['Private-Token'] = token

  req_options = {
    use_ssl: uri.scheme == 'https'
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end

  if response.code != '200'
    @logger.fatal("Error while attempting to fetch MR details from URL #{uri}: #{response.body}")
    exit(1)
  end

  !JSON.parse(response.body).empty?
end

ACCESS_TOKEN_FILE = '.gitlab-access-token'

unless File.exist?(ACCESS_TOKEN_FILE)
  puts 'Error: create a .gitlab-access-token fle in the current directory containing your personal access token.'
  exit 1
end

private_token = File.read(ACCESS_TOKEN_FILE).strip
Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = private_token
end

@config[:projects].each do |project|
  @logger.debug "Updating project '#{project[:path]}' with branch '#{project[:branch]}'"

  project_id = fetch_project_id(project[:path])
  ref = project[:branch]

  if mr_exists(token: private_token,
               project_id: project_id,
               mr_author: @config[:mr][:author],
               mr_title: @config[:mr][:title],
               branch_name: @config[:commit_branch])
    @logger.debug("An open MR with the title '#{mr_title}' already exists for project, skipping MR creation.")
    next
  end

  repo_path = checkout_path(project_name: project[:path])
  shell(cmd: "git clone -b #{ref} git@gitlab.com:#{project[:path]} #{repo_path}")
  shell(cmd: "git checkout -b #{@config[:commit_branch]}", workdir: repo_path)
  shell(cmd: "git push -u origin #{@config[:commit_branch]}", workdir: repo_path)

  mr_options = {
    source_branch: @config[:commit_branch],
    target_branch: ref,
    description: @config[:mr][:description],
    remove_source_branch: true
  }
  mr = Gitlab.create_merge_request(project_id, @config[:mr][:title], mr_options)
  mr_number = mr['web_url'].split('/').last.to_i

  files_to_stage = @config[:modify_fn].call(project_name: project[:path],
                                            branch: @config[:commit_branch],
                                            mr_number: mr_number,
                                            root_dir: repo_path)
  next if files_to_stage.empty?

  shell(cmd: "git add #{files_to_stage.join(' ')}", workdir: repo_path)

  shell(cmd: "git commit -m '#{@config[:commit_message]}'", workdir: repo_path)

  shell(cmd: "git push origin #{@config[:commit_branch]}", workdir: repo_path)

  @logger.info("MR created for '#{project[:path]}'. Link: #{mr['web_url']}")
end
